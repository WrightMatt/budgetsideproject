class UpdateTransactionCtrl

  constructor: (@$log, @$location, @$routeParams, @UserService) ->
      @$log.debug "constructing UpdateTransactionController"
      @transaction = {}
      @findTransaction()

  updateTransaction: () ->
      @$log.debug "updateTransaction()"
      @transaction.active = true
      @$log.debug "updatetransaction data #{@transaction}"
      @$log.debug "route params: #{@$routeParams.transactionName}, #{@$routeParams.description}, #{@$routeParams.totalCost}"
      @UserService.updateTransaction(@$routeParams.transactionName, @$routeParams.description, @$routeParams.totalCost ,@transaction)
      .then(
          (data) =>
            @$log.debug "Promise returned #{data} Transaction"
            @transaction = data
            @$location.path("/")
        ,
        (error) =>
            @$log.error "Unable to update Transaction: #{error}"
      )

  findTransaction: () ->
      # route params must be same name as provided in routing url in app.coffee
      transactionName = @$routeParams.transactionName
      description = @$routeParams.description
      @$log.debug "findUser route params: #{transactionName} #{description}"

      @UserService.listTransactions()
      .then(
        (data) =>
          @$log.debug "Promise returned #{data.length} Transactions"

          # find a user with the name of firstName and lastName
          # as filter returns an array, get the first object in it, and return it
          @transaction = (data.filter (transaction) -> transaction.transactionName is transactionName and transaction.description is description)[0]
      ,
        (error) =>
          @$log.error "Unable to get Transaction: #{error}"
      )

controllersModule.controller('UpdateTransactionCtrl', ['$log', '$location', '$routeParams', 'UserService', UpdateTransactionCtrl])