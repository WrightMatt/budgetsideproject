
class UserCtrl

    constructor: (@$log, @UserService) ->
        @$log.debug "constructing UserController"
        @users = []
        @getAllUsers()
        @transactions= []
        @getAllTransactions()
        @transactionTotal = 0.0
        @getTransactionTotal()

    getAllUsers: () ->
        @$log.debug "getAllUsers()"

        @UserService.listUsers()
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Users"
                @users = data
            ,
            (error) =>
                @$log.error "Unable to get Users: #{error}"
            )

    getAllTransactions: () ->
        @$log.debug "getAllTransactions()"

        @UserService.listTransactions()
        .then(
            (data) =>
                @$log.debug "Promise returned #{data.length} Transactions"
                @transactions = data
        ,
            (error) =>
                @$log.error "Unable to get Transactions: #{error}"
        )

    getTransactionTotal: () ->
        @$log.debug "getTransactionTotal()"
        @UserService.getTransactionTotal()
        .then(
          (data) =>
              @$log.debug "Promise returned #{data}"
              @transactionTotal = data
        ,
          (error) =>
              @$log.error "Unable to get total #{error}"
        )




controllersModule.controller('UserCtrl', ['$log', 'UserService', UserCtrl])