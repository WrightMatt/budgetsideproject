
class UserService

  @headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
  @defaultConfig = { headers: @headers }

  constructor: (@$log, @$http, @$q) ->
    @$log.debug "constructing UserService"

  listUsers: () ->
    @$log.debug "listUsers()"
    deferred = @$q.defer()

    @$http.get("/users")
    .success((data, status, headers) =>
      @$log.info("Successfully listed Users - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list Users - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  createUser: (user) ->
    @$log.debug "createUser #{angular.toJson(user, true)}"
    deferred = @$q.defer()

    @$http.post('/user', user)
    .success((data, status, headers) =>
      @$log.info("Successfully created User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to create user - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  updateUser: (firstName, lastName, user) ->
    @$log.debug "updateUser #{angular.toJson(user, true)}"
    deferred = @$q.defer()

    @$http.put("/user/#{firstName}/#{lastName}", user)
    .success((data, status, headers) =>
      @$log.info("Successfully updated User - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, header) =>
      @$log.error("Failed to update user - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  listTransactions: () ->
    @$log.debug "listTransactions()"
    deferred = @$q.defer()

    @$http.get("/transactions")
    .success((data, status, headers) =>
      @$log.info("Successfully listed Transactions - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to list Transactions - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  createTransaction: (transaction) ->
    @$log.debug "createTransaction #{angular.toJson(transaction, true)}"
    deferred = @$q.defer()

    @$http.post('/user/transactions', transaction)
    .success((data, status, headers) =>
      @$log.info("Successfully created transaction - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, headers) =>
      @$log.error("Failed to create transaction - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  updateTransaction: (transactionName, description, totalCost, transaction) ->
    transaction.active = true
    @$log.debug "updateTransaction #{angular.toJson(transaction, true)}"
    deferred = @$q.defer()

    @$http.put("/user/transaction/#{transactionName}/#{description}/#{totalCost}", transaction)
    .success((data, status, headers) =>
      @$log.info("Successfully updated Transaction - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, header) =>
      @$log.error("Failed to update Transaction - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

  getTransactionTotal: () ->
    @$log.debug "getTransactionTotal"
    deferred = @$q.defer()

    @$http.get("/user/transaction/total")
    .success((data, status, headers) =>
      @$log.info("Successfully retrieved Transaction total - status #{status}")
      deferred.resolve(data)
    )
    .error((data, status, header) =>
      @$log.error("Failed to retrieve Transaction total - status #{status}")
      deferred.reject(data)
    )
    deferred.promise

servicesModule.service('UserService', ['$log', '$http', '$q', UserService])