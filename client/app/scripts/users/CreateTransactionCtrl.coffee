
class CreateTransactionCtrl

    constructor: (@$log, @$location,  @UserService) ->
        @$log.debug "constructing CreateTransactionController"
        @transaction = {}
        @transactionId = 0

    createTransaction: () ->
        @$log.debug "createTransaction()"

        @UserService.listTransactions()
        .then(
            (data) =>
                @transaction.transactionId = parseInt(data.length)
                @transaction.active = true
                @UserService.createTransaction(@transaction)
                .then(
                    (data) =>
                        @$log.debug "Promise returned #{data} Transactions"
                        @transaction = data
                        @$location.path("/")
                    ,
                (error) =>
                    @$log.error "Unable to create Transaction: #{error}"
                )
        )
controllersModule.controller('CreateTransactionCtrl', ['$log', '$location', 'UserService', CreateTransactionCtrl])