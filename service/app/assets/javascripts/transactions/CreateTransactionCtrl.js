(function() {
    var CreateTransactionCtrl;

    CreateTransactionCtrl = (function() {
        function CreateTransactionCtrl($log, $location, UserService) {
            this.$log = $log;
            this.$location = $location;
            this.UserService = UserService;
            this.$log.debug("constructing CreateTransactionController");
            this.transaction = {};
            this.transactionId = 0;
        }

        CreateTransactionCtrl.prototype.createTransaction = function() {
            this.$log.debug("createTransaction()");
            return this.UserService.listTransactions().then((function(_this) {
                return function(data) {
                    _this.transaction.transactionId = parseInt(data.length);
                    _this.transaction.active = true;
                    return _this.UserService.createTransaction(_this.transaction).then(function(data) {
                        _this.$log.debug("Promise returned " + data + " Transactions");
                        _this.transaction = data;
                        return _this.$location.path("/");
                    }, function(error) {
                        return _this.$log.error("Unable to create Transaction: " + error);
                    });
                };
            })(this));
        };

        return CreateTransactionCtrl;

    })();

    controllersModule.controller('CreateTransactionCtrl', ['$log', '$location', 'UserService', CreateTransactionCtrl]);

}).call(this);

//# sourceMappingURL=CreateTransactionCtrl.js.map
