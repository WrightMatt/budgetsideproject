angular.module('myApp')
    .controller('DetailCtrl', function ($scope, $log,$modal ,TransactionService) {
        $scope.transactionTotal = 0.0;
        $scope.transactions = [];
        $scope.transactionCategories = [];
        $scope.max = 200;
        $scope.dynamic = 60;

        this.init = function() {
            $log.debug("initializing ChartCtrl");
            this.getTransactionCategories();
            this.getAllTransactions();
            this.getTransactionTotal();
        };

        this.getAllTransactions = function(){
            TransactionService.listTransactions().then(function(data){
                $log.debug("Promise returned these transactions: "+ data );
                $scope.transactions = data;
            }, function(error){
                $log.error("Unable to get Transactions: " + error);
            })
        };

        this.getTransactionTotal = function() {
            TransactionService.getTransactionTotal().then(function (data) {
                    $log.debug("Promise returned this total: " + data);
                    $scope.transactionTotal = data;
                $log.debug("$scope.transactionTotal updated to this total: " + $scope.transactionTotal);
            }, function (error) {
                $log.error("Unable to get Transactions total: " + error);
            })
        };

        this.getTransactionCategories = function() {
            TransactionService.getTransactionCategories().then(function(data){
               $log.debug("Promise returned these categories: " + data);
                $scope.transactionCategories = data;
            }, function(error){
                $log.error("Unable to get Transaction categories: " + error);
            })
        };

        this.showModal = function(category,total){
            $log.debug("passed category: " + category + "with total: " + total);
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: '/assets/partials/moreDetailsModal.html',
                controller: 'DetailModalInstanceCtrl',
                resolve: {
                    items: function () {
                        return {"category" : category,
                                "transactionTotal": $scope.transactionTotal,
                                "categoryTotal": total};
                        }
                }
            })

        };

        this.init();
    });


