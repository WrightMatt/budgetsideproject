(function() {
    var TransactionCtrl;

    TransactionCtrl = (function() {
        function TransactionCtrl($log, TransactionService) {
            this.$log = $log;
            this.TransactionService = TransactionService;
            this.$log.debug("constructing TransactionController");
            this.transactions = [];
            this.getAllTransactions();
            this.transactionTotal = 0.0;
            this.getTransactionTotal();
        }

        TransactionCtrl.prototype.getAllTransactions = function() {
            this.$log.debug("getAllTransactions()");
            return this.TransactionService.listTransactions().then((function(_this) {
                return function(data) {
                    _this.$log.debug("Promise returned " + data.length + " Transactions");
                    return _this.transactions = data;
                };
            })(this), (function(_this) {
                return function(error) {
                    return _this.$log.error("Unable to get Transactions: " + error);
                };
            })(this));
        };

        TransactionCtrl.prototype.getTransactionTotal = function() {
            this.$log.debug("getTransactionTotal()");
            return this.TransactionService.getTransactionTotal().then((function(_this) {
                return function(data) {
                    _this.$log.debug("Promise returned " + data);
                    return _this.transactionTotal = data;
                };
            })(this), (function(_this) {
                return function(error) {
                    return _this.$log.error("Unable to get total " + error);
                };
            })(this));
        };

        return TransactionCtrl;

    })();

    controllersModule.controller('TransactionCtrl', ['$log', 'TransactionService',TransactionCtrl]);

}).call(this);

//# sourceMappingURL=TransactionCtrl.js.map
