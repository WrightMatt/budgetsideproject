(function() {
    var UpdateTransactionCtrl;

    UpdateTransactionCtrl = (function() {
        function UpdateTransactionCtrl($log, $location, $routeParams, UserService) {
            this.$log = $log;
            this.$location = $location;
            this.$routeParams = $routeParams;
            this.UserService = UserService;
            this.$log.debug("constructing UpdateTransactionController");
            this.transaction = {};
            this.findTransaction();
        }

        UpdateTransactionCtrl.prototype.updateTransaction = function() {
            this.$log.debug("updateTransaction()");
            this.transaction.active = true;
            this.$log.debug("updatetransaction data " + this.transaction);
            this.$log.debug("route params: " + this.$routeParams.transactionName + ", " + this.$routeParams.description + ", " + this.$routeParams.totalCost);
            return this.UserService.updateTransaction(this.$routeParams.transactionName, this.$routeParams.description, this.$routeParams.totalCost, this.transaction).then((function(_this) {
                return function(data) {
                    _this.$log.debug("Promise returned " + data + " Transaction");
                    _this.transaction = data;
                    return _this.$location.path("/");
                };
            })(this), (function(_this) {
                return function(error) {
                    return _this.$log.error("Unable to update Transaction: " + error);
                };
            })(this));
        };

        UpdateTransactionCtrl.prototype.findTransaction = function() {
            var description, transactionName;
            transactionName = this.$routeParams.transactionName;
            description = this.$routeParams.description;
            this.$log.debug("findUser route params: " + transactionName + " " + description);
            return this.UserService.listTransactions().then((function(_this) {
                return function(data) {
                    _this.$log.debug("Promise returned " + data.length + " Transactions");
                    return _this.transaction = (data.filter(function(transaction) {
                        return transaction.transactionName === transactionName && transaction.description === description;
                    }))[0];
                };
            })(this), (function(_this) {
                return function(error) {
                    return _this.$log.error("Unable to get Transaction: " + error);
                };
            })(this));
        };

        return UpdateTransactionCtrl;

    })();

    controllersModule.controller('UpdateTransactionCtrl', ['$log', '$location', '$routeParams', 'UserService', UpdateTransactionCtrl]);

}).call(this);

//# sourceMappingURL=UpdateTransactionCtrl.js.map
