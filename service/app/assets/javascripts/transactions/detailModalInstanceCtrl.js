/**
 * Created by matt.wright on 2/9/16.
 */
angular.module('myApp')
    .controller('DetailModalInstanceCtrl', function ($scope, $modalInstance, $log, TransactionService, items) {
        $scope.title = items.category;
        $scope.transactionTotal = items.transactionTotal;
        $scope.transactionsOfCategory = [];
        $scope.categoryTotal = items.categoryTotal;
        $scope.percentOfTotal = ((items.categoryTotal/items.transactionTotal)*100).toPrecision(4);

        this.init = function() {
            this.getTransactionsByCategory(items.category);
        }

        $scope.close = function () {
            $modalInstance.dismiss('cancel');
        };
        this.getTransactionsByCategory = function(category){
            TransactionService.getTransactionsByCategory(category).then(function(data){
                $log.debug("Promise returned these transactions: "+ data +" with size: "+ data.length);
                $scope.transactionsOfCategory = data;
            }, function(error){
                $log.error("Unable to get Transactions: " + error);
            })
        };


        this.init();
});