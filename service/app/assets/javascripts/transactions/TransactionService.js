(function() {
  var TransactionService;

  TransactionService = (function() {
    TransactionService.headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };

    TransactionService.defaultConfig = {
      headers: TransactionService.headers
    };

    function TransactionService($log, $http, $q) {
      this.$log = $log;
      this.$http = $http;
      this.$q = $q;
      this.$log.debug("constructing TransactionService");
    }

    TransactionService.prototype.listTransactions = function() {
      var deferred;
      this.$log.debug("listTransactions()");
      deferred = this.$q.defer();
      this.$http.get("/transactions").success((function(_this) {
        return function(data, status, headers) {
          _this.$log.info("Successfully listed Transactions - status " + status);
          _this.$log.debug("Successfully listed Transactions: " + data);
          return deferred.resolve(data);
        };
      })(this)).error((function(_this) {
        return function(data, status, headers) {
          _this.$log.error("Failed to list Transactions - status " + status);
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };

    TransactionService.prototype.createTransaction = function(transaction) {
      var deferred;
      this.$log.debug("createTransaction " + (angular.toJson(transaction, true)));
      deferred = this.$q.defer();
      this.$http.post('/user/transactions', transaction).success((function(_this) {
        return function(data, status, headers) {
          _this.$log.info("Successfully created transaction - status " + status);
          return deferred.resolve(data);
        };
      })(this)).error((function(_this) {
        return function(data, status, headers) {
          _this.$log.error("Failed to create transaction - status " + status);
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };

    TransactionService.prototype.updateTransaction = function(transactionName, description, totalCost, transaction) {
      var deferred;
      transaction.active = true;
      this.$log.debug("updateTransaction " + (angular.toJson(transaction, true)));
      deferred = this.$q.defer();
      this.$http.put("/user/transaction/" + transactionName + "/" + description + "/" + totalCost, transaction).success((function(_this) {
        return function(data, status, headers) {
          _this.$log.info("Successfully updated Transaction - status " + status);
          return deferred.resolve(data);
        };
      })(this)).error((function(_this) {
        return function(data, status, header) {
          _this.$log.error("Failed to update Transaction - status " + status);
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };

    TransactionService.prototype.getTransactionTotal = function() {
      var deferred;
      this.$log.debug("getTransactionTotal");
      deferred = this.$q.defer();
      this.$http.get("/user/transaction/total").success((function(_this) {
        return function(data, status, headers) {
          _this.$log.info("Successfully retrieved Transaction total - status " + status);
          return deferred.resolve(data);
        };
      })(this)).error((function(_this) {
        return function(data, status, header) {
          _this.$log.error("Failed to retrieve Transaction total - status " + status);
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };

    TransactionService.prototype.getTransactionCategories = function() {
      var deferred;
      this.$log.debug("getAllTransactionCategories");
      deferred = this.$q.defer();
      this.$http.get("/user/transaction/categories").success((function(_this) {
        return function(data, status, headers) {
          _this.$log.info("Successfully retrieved Transaction categories - status " + status);
          return deferred.resolve(data);
        };
      })(this)).error((function(_this) {
        return function(data, status, header) {
          _this.$log.error("Failed to retrieve Transaction categories - status " + status);
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };


    TransactionService.prototype.getTransactionsByCategory = function(category) {
      var deferred;
      this.$log.debug("getTransactionsByCategory using" + category);
      deferred = this.$q.defer();
      this.$http.get("/user/transaction/details/"+ category).success((function(_this) {
        return function(data, status, headers) {
          _this.$log.info("Successfully retrieved Transactions by category - status " + status);
          return deferred.resolve(data);
        };
      })(this)).error((function(_this) {
        return function(data, status, header) {
          _this.$log.error("Failed to retrieve Transaction categories - status " + status);
          return deferred.reject(data);
        };
      })(this));
      return deferred.promise;
    };

    return TransactionService;

  })();

  servicesModule.service('TransactionService', ['$log', '$http', '$q', TransactionService]);

}).call(this);

//# sourceMappingURL=TransactionService.js.map
