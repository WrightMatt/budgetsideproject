/**
 * Created by matt.wright on 2/5/16.
 */
'use strict';

angular.module('myApp')
    .service('ChartService', function($log, $http, $q) {

            this.getPieData = function() {
                    $log.debug("generatePieData()");

                    return $http.get("/user/transaction/categories/pie").then(function(response, status, headers) {

                            $log.info("Successfully generated data - status " + status);
                            $log.debug("Successfully generated pie: " + JSON.stringify(response.data));
                            $log.debug("Successfully generated pie:status " + JSON.stringify(status));
                            $log.debug("Successfully generated pie:header " + JSON.stringify(headers));
                            return response.data;

                    }, function(error, status, headers) {
                            $log.error("Failed to generate pie - status " + status);
                            throw error;
                    });
            };
    })
    .controller('ChartCtrl',function ($scope, $log, ChartService) {
            $log.debug("initializing ChartCtrl");
            $scope.pieData = [];
            ChartService.getPieData().then(function(data){
                    $log.debug("Promise returned pie with: " + JSON.stringify(data));
                    $scope.pieData = data;
                    $scope.ctx = document.getElementById("chart-area").getContext("2d");

                    new Chart($scope.ctx).Pie($scope.pieData)
            },function(error){
                    $log.debug("Promise failed to return pie with: " + error);
            });
    });
