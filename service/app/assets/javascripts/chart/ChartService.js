/**
 * Created by matt.wright on 2/4/16.

var ChartService = angular.module('ChartService');
    ChartService.factory('ChartDataOp',  ['$http', function($http) {
        var ChartDataOp = {};

        ChartDataOp.generatePieData = function() {
            var pieData = [
                {
                    value: 110.95,
                    color: "#F7464A",
                    highlight: "#FF5A5E",
                    label: "Games"
                },
                {
                    value: 35.57,
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: "Transportation"
                },
                {
                    value: 26.66,
                    color: "#FDB45C",
                    highlight: "#FFC870",
                    label: "Food"
                }

            ];
            return pieData;
        };
        return ChartDataOp;
    }]);

//(function() {
//    var ctx = document.getElementById("chart-area").getContext("2d");
//    window.mypie = new Chart(ctx).Pie(pieData);
//})();
//

 */