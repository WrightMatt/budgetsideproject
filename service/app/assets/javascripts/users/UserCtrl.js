(function() {
  var UserCtrl;

  UserCtrl = (function() {
    function UserCtrl($log, UserService) {
      this.$log = $log;
      this.UserService = UserService;
      this.$log.debug("constructing UserController");
      this.transactions = [];
      this.getAllTransactions();
      this.transactionTotal = 0.0;
      this.getTransactionTotal();
    }

    UserCtrl.prototype.getAllUsers = function() {
      this.$log.debug("getAllUsers()");
      return this.UserService.listUsers().then((function(_this) {
        return function(data) {
          _this.$log.debug("Promise returned " + data.length + " Users");
          return _this.users = data;
        };
      })(this), (function(_this) {
        return function(error) {
          return _this.$log.error("Unable to get Users: " + error);
        };
      })(this));
    };

    UserCtrl.prototype.getAllTransactions = function() {
      this.$log.debug("getAllTransactions()");
      return this.UserService.listTransactions().then((function(_this) {
        return function(data) {
          _this.$log.debug("Promise returned " + data.length + " Transactions");
          return _this.transactions = data;
        };
      })(this), (function(_this) {
        return function(error) {
          return _this.$log.error("Unable to get Transactions: " + error);
        };
      })(this));
    };

    UserCtrl.prototype.getTransactionTotal = function() {
      this.$log.debug("getTransactionTotal()");
      return this.UserService.getTransactionTotal().then((function(_this) {
        return function(data) {
          _this.$log.debug("Promise returned " + data);
          return _this.transactionTotal = data;
        };
      })(this), (function(_this) {
        return function(error) {
          return _this.$log.error("Unable to get total " + error);
        };
      })(this));
    };

    return UserCtrl;

  })();

  controllersModule.controller('UserCtrl', ['$log', 'UserService', UserCtrl]);

}).call(this);

//# sourceMappingURL=UserCtrl.js.map
