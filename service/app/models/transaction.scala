package models

case class Transaction(

                        transactionName: String,
                        description: String,
                        category: String,
                        totalCost: Double,
                        active: Boolean,
                        transactionId: Int
                      )

/*object JsonFormats {
  import play.api.libs.json.Json

  // Generates Writes and Reads for Feed and User thanks to Json Macros
  implicit val userFormat = Json.format[Transaction]
}*/