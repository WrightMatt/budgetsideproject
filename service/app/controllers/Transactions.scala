package controllers

import javax.inject.Singleton

import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json._
import play.api.mvc._
import play.modules.reactivemongo.MongoController
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.api.Cursor

import scala.collection.mutable
import scala.concurrent.{Promise, Future}


/**
  * The Users controllers encapsulates the Rest endpoints and the interaction with the MongoDB, via ReactiveMongo
  * play plugin. This provides a non-blocking driver for mongoDB as well as some useful additions for handling JSon.
  *
  * @see https://github.com/ReactiveMongo/Play-ReactiveMongo
  */
@Singleton
class Transactions extends Controller with MongoController {

  private final val logger: Logger = LoggerFactory.getLogger(classOf[Transactions])

  /*
   * Get a JSONCollection (a Collection implementation that is designed to work
   * with JsObject, Reads and Writes.)
   * Note that the `collection` is not a `val`, but a `def`. We do _not_ store
   * the collection reference to avoid potential problems in development with
   * Play hot-reloading.
   */
  def collection: JSONCollection = db.collection[JSONCollection]("transactions")

  // ------------------------------------------ //
  // Using case classes + Json Writes and Reads //
  // ------------------------------------------ //

  import models.JsonFormats._
  import models._

  def createTransaction = Action.async(parse.json) {
    request =>
      /*
       * request.body is a JsValue.
       * There is an implicit Writes that turns this JsValue as a JsObject,
       * so you can call insert() with this JsValue.
       * (insert() takes a JsObject as parameter, or anything that can be
       * turned into a JsObject using a Writes.)
       */
      request.body.validate[Transaction].map {
        transaction =>
          // `transaction` is an instance of the case class `models.Transaction`
          collection.insert(transaction).map {
            lastError =>
              logger.debug(s"Successfully inserted with LastError: $lastError")
              Created(s"Transaction Created")
          }
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

  def updateTransaction(transactionName: String, description: String, totalCost: Double ) = Action.async(parse.json) {
    request =>
      request.body.validate[Transaction].map {
        transaction =>
          // find our transaction by Transaction name and details
          val nameSelector = Json.obj("transactionName" -> transactionName, "description" -> description, "totalCost" ->totalCost, "active" -> true)
          collection.update(nameSelector, transaction).map {
            lastError =>
              logger.debug(s"Successfully updated with LastError: $lastError")
              Created(s"Transaction Updated")
          }
      }.getOrElse(Future.successful(BadRequest("invalid json")))
  }

  def findTransactions = Action.async {
    // let's do our query
    val cursor: Cursor[Transaction] = collection.
      // find all
      find(Json.obj("active" -> true)).
      // sort them by creation date
      sort(Json.obj("created" -> -1)).
      // perform the query and get a cursor of JsObject
      cursor[Transaction]

    // gather all the JsObjects in a list
    val futureTransactionList: Future[List[Transaction]] = cursor.collect[List]()

    // transform the list into a JsArray
    val futureTransactionsJsonArray: Future[JsArray] = futureTransactionList.map { transactions =>
      Json.arr(transactions)
    }
    // everything's ok! Let's reply with the array
    futureTransactionsJsonArray.map {
      transactions =>
        Ok(transactions(0))
    }
  }

  def getTransactionCategories = Action.async {
    // query for transactions
    val cursor: Cursor[Transaction] = collection.
      // find all
      find(Json.obj("active" -> true)).
      // perform the query and get a cursor of JsObject
      cursor[Transaction]

    def getTransactionCategories(transactionArray: Future[List[Transaction]] ) :Future[List[String]] = {

      val categories = Promise[List[String]]()

      Future {
        for (transactionJson <- transactionArray) {
          var tempList = List[String]()
          for (transactionX <- transactionJson) {
            if( !tempList.contains(transactionX.category)) {
              tempList = tempList :+ transactionX.category }
          }
          println(tempList)
          categories.success(tempList)
        }
      }
      categories.future
    }


    // gather all the JsObjects in a list
    val futureTransactionList: Future[List[Transaction]] = cursor.collect[List]()
    val transactionCategories = getTransactionCategories(futureTransactionList)
    // everything's ok! Let's reply with the array
    transactionCategories.map {
      transactionCategory =>
        Ok(Json.toJson(transactionCategory))
    }
  }


  def findTransactionsByCategory(category: String) = Action.async {
    // let's do our query
    val cursor: Cursor[Transaction] = collection.
      // find all
      find(Json.obj("category" -> category)).
      // sort them by creation date
      sort(Json.obj("created" -> -1)).
      // perform the query and get a cursor of JsObject
      cursor[Transaction]

    // gather all the JsObjects in a list
    val futureTransactionList: Future[List[Transaction]] = cursor.collect[List]()

    // transform the list into a JsArray
    val futureTransactionsJsonArray: Future[JsArray] = futureTransactionList.map { transactions =>
      Json.arr(transactions)
    }
    print("found some things")
    // everything's ok! Let's reply with the array
    futureTransactionsJsonArray.map {
      transactions =>
        Ok(transactions(0))
    }
  }


  def getTransactionsTotal = Action.async  {
    // query for transactions
    val cursor: Cursor[Transaction] = collection.
      // find all
      find(Json.obj("active" -> true)).
      // perform the query and get a cursor of JsObject
      cursor[Transaction]

    // gather all the JsObjects in a list
    val futureTransactionList: Future[List[Transaction]] = cursor.collect[List]()
    val transactionTotal = addTransactionsTotal(futureTransactionList)
    // everything's ok! Let's reply with the array
    transactionTotal.map {
      transactionTotals =>
        Ok(Json.toJson(transactionTotals))
    }
  }

  def getTransactionsCategoryWithTotals = Action.async  {
    // query for transactions
    val cursor: Cursor[Transaction] = collection.
      // find all
      find(Json.obj("active" -> true)).
      // perform the query and get a cursor of JsObject
      cursor[Transaction]

    //returns a list of categories
    def getTransactionCategories(transactionArray: Future[List[Transaction]] ) :Future[List[String]] = {
      val categories = Promise[List[String]]()
      Future {
        for (transactionJson <- transactionArray) {

          var tempList = List[String]()
          for (transactionX <- transactionJson) {
            if( !tempList.contains(transactionX.category)) {
              tempList = tempList :+ transactionX.category }
          }
          println(tempList)
          categories.success(tempList)
        }
      }
      categories.future
    }


    // gather all the JsObjects in a list
    val futureTransactionList: Future[List[Transaction]] = cursor.collect[List]()
    //filter out the transaction categories
    val transactionCategories = getTransactionCategories(futureTransactionList)
    //get transactions by category
    val transactionCategoryTotals = findTransactionsTotalsFromCategories(transactionCategories)
    //get total of each category


    // everything's ok! Let's reply with the array
    transactionCategoryTotals.map {
      transactionCategory =>
        Ok(Json.toJson(transactionCategory))
    }
  }

  //Helper Functions
  def addTransactionsTotal(transactionArray: Future[List[Transaction]] ) :Future[Double] = {
    transactionArray.map { transactionJson =>
      transactionJson.map { transaction =>
        transaction.totalCost
      }.sum
    }
  }

  def findTransactionsTotalsFromCategories(futureCategories: Future[List[String]]): Future[List[JsValue]] = {
    //Maps Future List to List "categories"
    futureCategories.flatMap { categories =>
      println(s"inputted Future List: $categories")
      //Maps List "categories" to each category
      val listFutureCategoryTotalJson = categories.map { category =>
        println(s"obtaining Transactions for: $category")
        //Queries to find each transaction from the current category
        val futureTransactions = collection.find(Json.obj("category" -> category)).cursor[Transaction].collect[List]()
        //Loops through the retrieved transactions and maps them to the JSon
        futureTransactions.foreach { transactions =>
          println(s"retrieved: $transactions")
        }
        addTransactionsTotal(futureTransactions).map { total =>
          Json.toJson(
            Map(
              "category" -> Json.toJson(category),
              "Total" -> Json.toJson(total)
            ))
        }
      }
      Future.sequence(listFutureCategoryTotalJson)
    }
  }

  def getTransactionsCategoryPieFormat = Action.async  {
    // query for transactions
    val cursor: Cursor[Transaction] = collection.
      // find all
      find(Json.obj("active" -> true)).
      // perform the query and get a cursor of JsObject
      cursor[Transaction]
    //returns a list of categories
    def getTransactionCategories(transactionArray: Future[List[Transaction]] ) :Future[List[String]] = {
      val categories = Promise[List[String]]()
      Future {
        for (transactionJson <- transactionArray) {

          var tempList = List[String]()
          for (transactionX <- transactionJson) {
            if( !tempList.contains(transactionX.category)) {
              tempList = tempList :+ transactionX.category }
          }
          println(tempList)
          categories.success(tempList)
        }
      }
      categories.future
    }


    // gather all the JsObjects in a list
    val futureTransactionList: Future[List[Transaction]] = cursor.collect[List]()
    //filter out the transaction categories
    val transactionCategories = getTransactionCategories(futureTransactionList)
    //get transactions by category
    val transactionCategoryTotals = formatCategories(transactionCategories)


    // everything's ok! Let's reply with the array
    transactionCategoryTotals.map {
      transactionCategory =>
        Ok(Json.toJson(transactionCategory))
    }
  }

  def formatCategories(futureCategories: Future[List[String]]): Future[List[JsValue]] = {
    //Maps Future List to List "categories"
    futureCategories.flatMap { categories =>
      println(s"inputted Future List: $categories with size "+ categories.size)
      //Maps List "categories" to each category
      val listFutureCategoryTotalJson = categories.map { category =>
        println(s"obtaining Transactions for: $category")
        //Queries to find each transaction from the current category
        val futureTransactions = collection.find(Json.obj("category" -> category)).sort(Json.obj("category" -> -1)).cursor[Transaction].collect[List]()
        //Loops through the retrieved transactions and maps them to the JSon
        futureTransactions.foreach { transactions =>
          println(s"retrieved: $transactions")
        }
        addTransactionsTotal(futureTransactions).map { total =>
          Json.toJson(
            Map(
              "label" -> Json.toJson(category),
              "value" -> Json.toJson(total)
            ))
        }
      }
      val colors = Seq("#6699FF","#9933FF","#F7464A","#FF9933","#FFFF00","#00FF00")
      val nextColor = Stream.iterate(0)(x => (x + 1) % colors.length).map(x => colors(x))
      Future.sequence(listFutureCategoryTotalJson).map { categoryTotalsJson =>
        categoryTotalsJson.zip(nextColor).map {
          case (jsObject: JsObject, color: String) => jsObject + ("color", Json.toJson(color))
        }
      }
    }
  }
}

